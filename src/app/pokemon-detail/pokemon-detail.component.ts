import { Component, OnInit } from '@angular/core';
import { PokeCenterService } from '../poke-center.service'
import { PokemonMovesComponent} from '../pokemon-moves/pokemon-moves.component'

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {
  pokeman;
  moves=[];
  move;
  name;

  constructor(private pokeCenter: PokeCenterService) { }

  async ngOnInit() {
    try{
      let curr_url=window.location.href
      let a=curr_url.split("/") //a little too hacky :/
      const resp:any =await this.pokeCenter.sendGetRequest("http://pokeapi.co/api/v2/pokemon/"+a[4] )
      this.name=a[4]
      this.pokeman=resp;
      this.moves=resp.moves;
      this.move=this.moves[1];


    }catch(error){console.error(error)}
  }

}
