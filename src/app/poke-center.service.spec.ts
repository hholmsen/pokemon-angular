import { TestBed } from '@angular/core/testing';

import { PokeCenterService } from './poke-center.service';

describe('PokeCenterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PokeCenterService = TestBed.get(PokeCenterService);
    expect(service).toBeTruthy();
  });
});
