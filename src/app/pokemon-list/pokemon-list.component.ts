import { Component, OnInit } from '@angular/core';
import { PokemonListItemComponent } from '../pokemon-list-item/pokemon-list-item.component'
import { PokeCenterService } from '../poke-center.service';


@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {
  pokelist=[];
  products=[];
  viewlist=[];
  next:String="";
  images=[];
  current_latest=0;
  isSearch=false;
  constructor(private pokeCenter: PokeCenterService) { }

  async ngOnInit() {
    try{
      const resp:any =await this.pokeCenter.sendGetRequest("http://pokeapi.co/api/v2/pokemon/")
     
      this.next = resp.next;
      for(let i =0;i<resp.results.length;i++){//probably a better solution for this.
        this.pokelist.push(resp.results[i])
      }

    }catch(error){console.error(error)}
    this.current_latest=0;
    this.fetchImages()
  }

  async fetchImages(){
    for(let i =this.current_latest; i<this.pokelist.length;i++){
      try{
        const resp:any =await this.pokeCenter.sendGetRequest(this.pokelist[i].url)
        this.images.push(resp.sprites.front_default)
        for(let i =this.current_latest;i<this.pokelist.length;i++){
          this.pokelist[i].image=this.images[i]
        }
        
        
      }catch(err){console.error(err)}
    }
    this.viewlist=this.pokelist
  }

  async LoadMore(){
    try{
      const resp:any =await this.pokeCenter.sendGetRequest(this.next)
      this.next = resp.next;
      for(let i =0;i<resp.results.length;i++){//probably a better solution for this.
        this.pokelist.push(resp.results[i])
      }
    }catch(error){console.error(error)}
    this.current_latest+=20;
    this.fetchImages()
    this.isSearch=false;
    this.viewlist=this.pokelist
  }

  handleChange(h){
    if(h.length>0){
      this.isSearch=true;
    }else{
      this.isSearch=false;
    }
    this.viewlist= this.pokelist.filter(s => s.name.indexOf(h) !== -1);


  }



}
