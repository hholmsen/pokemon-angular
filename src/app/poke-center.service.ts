import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class PokeCenterService {

  private REST_API_SERVER = "http://pokeapi.co/api/v2/pokemon/";

  constructor(private httpClient: HttpClient) { }

  public sendGetRequest(link):Promise<any>{
    return this.httpClient.get(link).toPromise();
  }
}